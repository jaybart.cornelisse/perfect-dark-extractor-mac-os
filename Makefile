CC := gcc-13

C_FILES := \
	src/main.c \
	src/gbi.c \
	src/ext_anims.c \
	src/ext_audio.c \
	src/ext_banners.c \
	src/ext_files.c \
	src/ext_file_audio.c \
	src/ext_file_bg.c \
	src/ext_file_lang.c \
	src/ext_file_model.c \
	src/ext_file_pads.c \
	src/ext_file_tiles.c \
	src/ext_fonts.c \
	src/ext_jpnfonts.c \
	src/ext_textures.c \
	src/util.c

build/pd-extract: $(C_FILES)
	mkdir -p $(@D)
	$(CC) -g -lz -o build/pd-extract $(C_FILES)

build/pd-extract-test: $(C_FILES)
	$(CC) -g -lz -DFORCE_BE32 -o build/pd-extract-test $(C_FILES)

clean:
	rm -f build/pd-extract

cleanall:
	rm -rf build

