# Perfect Dark Extractor

Extracts assets from a Perfect Dark ROM, then converts them into a format that's compatible with the host machine's architecture (ie. changing endianness and/or pointer size if needed).

## Build Instructions

Run `make`. The program will be compiled to `build/pd-extract`.

## Usage Instructions

Example usage:

    build/pd-extract pd.ntsc-final.z64 extracted/

The synopsis is:

    build/pd-extract <ROMFILE> [OUTPATH]

Where:

* `<ROMFILE>` is the filename of a Perfect Dark ROM.
* `[OUTPATH]` is the output directory to extract to. The trailing slash is optional. Defaults to `extracted/`.

The ROM version can be any of the final release versions (NTSC 1.0, NTSC 1.1, PAL and JPN). The betas are not supported.

The extractor will automatically detect the version of the ROM being used, and also detect if the ROM is byteswapped.

## Graphics Binary Interface (GBI)

Graphics display list commands are 64 bits wide on the N64 and some commands allow a 32-bit pointer to be passed to them. This presents a problem when compiled for machines which use 64-bit pointers as there is not enough space to pack a 64-bit pointer into the command. When compiled for an architecture that uses 64-bit pointers, every GBI command is changed to 128 bits instead. Commands which accept a pointer will have the pointer in the second double-word. They are otherwise the same. Note that endianness conversion also applies to GBI commands.
